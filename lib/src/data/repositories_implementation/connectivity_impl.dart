import 'package:connectivity_plus/connectivity_plus.dart';

import '../../domain/repositories/connectivity_repository.dart';
import '../services/remote/internet_checker.dart';

class ConnectivityImpl implements ConnectivityRepository {
  final Connectivity _connectivity;
  final InternetChecker _internetChecker;

  ConnectivityImpl(this._connectivity, this._internetChecker);

  @override
  // TODO: implement hasInternetConnection
  Future<bool> get hasInternetConnection async {
    final List<ConnectivityResult> connectivityResult =
        await _connectivity.checkConnectivity();

    // This condition is for demo purposes only to explain every connection type.
    // Use conditions which work for your requirements.
    if (connectivityResult.contains(ConnectivityResult.none)) {
      return false;
    }

    return _internetChecker.hasInternet();
  }
}
