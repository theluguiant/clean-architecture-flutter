import '../../domain/models/user.dart';
import '../../domain/repositories/Authentication_repository.dart';

class AuthenticationImpl implements AuthenticationRepository {
  @override
  // TODO: implement isSignedIn
  Future<bool> get isSignedIn async {
    return Future.value(true);
  }

  @override
  // TODO: implement userData
  Future<User?> getUserData() {
    return Future.value(User());
  }
}
