import 'package:flutter/material.dart';

import '../../../../../main.dart';
import '../../../routes/routes.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) => _init());
  }

  Future<void> _init() async {
    final injector = Injector.of(context);
    final conn = injector.connectivityRepository;
    final hasInternetConnection = await conn.hasInternetConnection;

    print('hola ++++++++ $hasInternetConnection');

    if (hasInternetConnection) {
      final authRepository = injector.authenticationRepository;
      final isSignedIn = await authRepository.isSignedIn;
      if (isSignedIn) {
        final user = await authRepository.getUserData();
        if (mounted) {
          if (user != null) {
            print('Usuario: $user');
            _goTo(Routes.home);
          } else {
            _goTo(Routes.singIn);
          }
        }
      } else if (mounted) {
        _goTo(Routes.singIn);
      }
    } else {
      _goTo(Routes.Offline);
    }
  }

  void _goTo(String routeName) {
    Navigator.pushReplacementNamed(context, routeName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: const SafeArea(
              child: Center(
                  child: SizedBox(
                      width: 80,
                      height: 80,
                      child: CircularProgressIndicator())))),
    );
  }
}
