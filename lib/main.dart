import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

import 'src/app.dart';
import 'src/data/repositories_implementation/authentication_impl.dart';
import 'src/data/repositories_implementation/connectivity_impl.dart';
import 'src/data/services/remote/internet_checker.dart';
import 'src/domain/repositories/Authentication_repository.dart';
import 'src/domain/repositories/connectivity_repository.dart';

void main() {
  runApp(Injector(
      connectivityRepository:
          ConnectivityImpl(Connectivity(), InternetChecker()),
      authenticationRepository: AuthenticationImpl(),
      child: const App()));
}

class Injector extends InheritedWidget {
  const Injector(
      {super.key,
      required super.child,
      required this.connectivityRepository,
      required this.authenticationRepository});

  final ConnectivityRepository connectivityRepository;
  final AuthenticationRepository authenticationRepository;

  @override
  bool updateShouldNotify(_) => false;

  static Injector of(BuildContext context) {
    final injector = context.dependOnInheritedWidgetOfExactType<Injector>();
    assert(injector != null,
        'No se pudo encontrar un Injector en el árbol de widgets');
    return injector!;
  }
}
